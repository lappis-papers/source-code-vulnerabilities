\section{Research Design}
\label{sec:research-design}

To guide the development of a reproducible method to define low complexity
functions that enable the longitudinal study of software flaws, we addressed
the following research question:

\textbf{Q1} -- \textit{Is it possible to define low complexity models to
predict flaws in the Linux source code?}

With low complexity functions, we are able to automate the continuous
monitoring of flaws in a simple way, without the need to build complex models,
which would be expensive to maintain. If we can easily infer the number of
flaws predicted in a software as well, improvements in the software development
cycle based on it become possible. We could, for example, make better decisions
on when to increase the efforts to fix bugs or refactor code.

\subsection{Data collection}

Although it is common to refer to the whole operating system as Linux, the name
refers to the kernel. Its development has been active for over 20 years and it
is written in the C programming language. The Linux source code showed itself
adequate for this study due to its size and the fact that it is widely used
around the world, which led us to believe there is a higher likelihood of
finding flaws in it. A total of 391 versions of the kernel were analyzed. These
version were downloaded from the project's official Git
repositories\footnote{\url{git.kernel.org}}. By the time of our first analyses,
the versions between 2.6.11 and 3.9 were available. After that, we have tested
the candidate models in three recent kernel versions: 4.0, 4.5, and 4.9.

The static analyzer selected was \texttt{Cppcheck}. According to the
description in its homepage\footnote{\url{cppcheck.sourceforge.net}}, it aims
to be a \textbf{sound} \cite{paul:2001} static analyzer, since its main goal is
to find real bugs in the source code, not generating false positives. The main
characteristic of sound static analyzers is the low rate of false positives.
The \texttt{Cppcheck} output is composed of a location (source code line) and a
message, describing a problem (warning or error). We ran \texttt{Cppcheck} 1.67
in each version of Linux and the corresponding output files for each of the
analyzed versions are publicly available at
\url{github.com/lucaskanashiro/linux-analysis}.

\subsection{Data analysis}

The risk of Linux versions with more modules to stand out compared to other
versions was identified upon analyzing the absolute number of flaws found.
Thus, the number of flaws was normalized based on the number of modules in each
version, resulting in the \textbf{flaw rate} per module, that is our
dependent variable. To analyze the influence of the project growth, we used
the \textbf{total number of modules} as our independent variable. In this
context, a kernel module is one C programming language source code file (header
files are not counted for this matter).

Since there are several known flaws, we have conducted a previours study with
10 popular Free Software projects \footnote{Bash, Blender, FFmpeg, Firefox,
Gstreamer, Inetutils, OpenSSH, OpenSSL,  Python2.7, and Ruby-2.1.} from
different domains to find the most recurrent flaws and select them to perform
the analyses for this work. Since this paper proposes to find one model per
flaw analyzed, we selected the two most frequent flaws found in those projects:
\textbf{NULL pointer dereference} and \textbf{use of uninitialized variable}.
Both are cataloged by MITRE and as Common Weakness Enumerations, the former as
CWE476 and the latter as CWE457.

\begin{figure}[hbt]
 \centering
 \begin{minipage}[hbt]{0.475\textwidth}
 \includegraphics[width=\textwidth]{images/plot_cwe476.eps}
 \caption{CWE 476 (Number of NULL pointer) dereferences evolution.}
 \label{fig:plot-cwe476}
 \end{minipage}
\end{figure} 

\begin{figure}
 \begin{minipage}[hbt]{0.475\textwidth}
  \includegraphics[width=\textwidth]{images/plot_cwe457.eps}
  \caption{CWE 457 (Use of uninitialized variable) dereferences evolution.}
  \label{fig:plot-cwe457}
 \end{minipage}
\end{figure}


As we can see in Figures \ref{fig:plot-cwe476} and \ref{fig:plot-cwe457}, in
the course of Linux evolution, the total number of flaws dereferences does not
always increase. It increases until a certain point and then there is a
tendency towards stabilization, which is, most likely, the result of the Linux
development process evolution. To unsderstand the behavior of the data we
performed some analyses, such as correlation matrix between the flaws and the
number of modules, using the Pearson correlation coefficient
\cite{rossman:1996}.

\begin{figure}[htb]                                                                                                               
 \centering                                                                     
 \begin{minipage}[b]{0.475\textwidth}
  \includegraphics[width=\textwidth]{images/cwe476-4-plot.eps}                 
  \caption{4-Plot for CWE 476 -- Number of NULL pointer.}       
  \label{fig:4-plot-cwe476}                                                      
 \end{minipage}
\end{figure}

\begin{figure}
 \begin{minipage}[b]{0.475\textwidth}
  \includegraphics[width=\textwidth]{images/cwe457-4-plot.eps}                 
  \caption{4-Plot for CWE 457 -- Use of uninitialized variable.}       
  \label{fig:4-plot-cwe457}                                                      
 \end{minipage}
\end{figure} 

Also, to confirm that the data can not be represented by a linear function, we
used the 4-plot technique developed by
NIST~\footnote{\url{itl.nist.gov/div898/handbook/eda/section3/eda3332.htm}}.
This technique facilitates the understanding of the data distribution and its
randomness. For this study, identifying these characteristics was necessary to
obtain information for the definition of a model which satisfies the studied
data set, or that could, at least, eliminate unwanted options. For Linux, the
data does not follow a normal distribution, but a long-tailed distribution, so
that it is not constant and varies widely, as we can see in Figures
\ref{fig:4-plot-cwe476} and \ref{fig:4-plot-cwe457}.

Both analyses showed that the flaw rates were not strongly correlated and they
did not fit a normal distribution, which corroborates with the fact that
exponential and weibull distributions can model the behavior of defect
occurrence in a wide range of software \cite{jones1991reliability,
li2004empirical,wood1996predicting,penta_evolution_2008}. With this in mind we
discarded the possibility of using a linear model.

Since our proposal is based on low complexity models and linear functions do
not fit the data set, we investigated polynomial models.  Initially, an
identification of outliers \cite{hawkins80} work was done for the definition of
the polynomial models through a technique presented by Tukey \cite{tukey:1977},
using a box plot. The outliers found were removed to improve the candidate
model accuracy. After that, parametric and non-parametric models were built.
The non-parametric model is a black box method from which it is not possible to
obtain a mathematical function. In this study, it is used as a base for the
definition of a parametric model. As a result, we have a function that receives
the total number of modules in the project as input and returns the flaw rate
as output.

We also have used the Locally Weighted Regression (LOESS) non-parametric method
to provide a smooth surface using a non-parametric regression
\cite{cleveland:1988}. In short, the LOESS method applies several minor
regressions to the data set as well as it can guide the definition of a
possible parametric model. Therefore, we have applied polynomial regressions to
obtain the parametric models definition for this study.

Posteriorly, to validate the obtained models, we used the K-Fold
cross-validation technique, which consists in dividing the data set in $K$
groups, where one of these groups is used to test the model and the other
groups are used to train the model \cite{richard:1984}. With this kind of
cross-validation, we can test the model with values from different versions of
the kernel, obtaining the model prediction error.  We use the mean squared
error, which guarantees the model reliability, given that this error has a low
value.  After the validation, the model errors were compared and we could chose
the best model, getting to low complexity (polynomial) functions capable of
predicting the Linux source code flaw rates proposed in this paper.

Finally, we have tested the candidate models collecting new data from recent
Linux versions (such as 4.0, 4.5, and 4.9) and comparing them to the
predicted values from our low complexity polynomial functions. 
