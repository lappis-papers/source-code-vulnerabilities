\section{Results}
\label{sec:results}

For each CWE studied, two models were defined and one was selected. Thus, it is
possible to use low complexity functions, with low error, to define a model to
predict flaws in Linux, answering our research question.  This is feasible when
we can obtain a meaningful data set, as it was the case for Linux, where we
used 391 different versions of the project.

In short, comparing and selecting the models for each CWE, we have Equations
\ref{eq:cwe476} and \ref{eq:cwe457} representing the selected cubic models:

\begin{footnotesize}
\begin{equation} \label{eq:cwe476}
\begin{split}
 tax\_CWE476(modules) & = (1.911224 * 10^{-15}) * modules^{3} \\
                      & - (1.72028 * 10^{-10}) * modules^{2} \\
                      & + (4.857479 * 10^{-6}) * modules \\
                      & - (0.03460173)
\end{split}
\end{equation}

\begin{equation} \label{eq:cwe457}
\begin{split}
 tax\_CWE457(modules) & = (-6.466983 * 10^{-16}) * modules^{3} \\
                      & + (5.603787 * 10^{-11}) * modules^{2} \\
                      & - (1.639652 * 10^{-6}) * modules \\
                      & + (0.02287291)
\end{split}
\end{equation}
\end{footnotesize}

\begin{table}[h]
\centering
\begin{footnotesize}
\begin{tabular}{cccc}
\hline
{Version}           & {tax\_CWE476} & {tax\_CWE457} & {Modules} \\ \hline
linux-v2.6.11       & 0.005735325   & 0.008992424   & 14123     \\ \hline
linux-v2.6.39       & 0.008927095   &    -          & 30245     \\ \hline
linux-v3.16-rc3     &   -           & 0.006577706   & 37095     \\ \hline
linux-v3.9-rc8      & 0.006460368   & 0.005663884   & 33899     \\ \hline
linux-v4.0.0	    & 0.006435337   & 0.006036167   & 37793     \\ \hline
linux-v4.5.0	    & 0.006829370   & 0.005503281   & 40209     \\ \hline
linux-v4.9.0	    & 0.007679160   & 0.004861321   & 42234     \\ \hline
\end{tabular}
  \caption{Some flaw rates and predictions.}
\label{tab:cwe}
\end{footnotesize}
\end{table}


It can be verified that the coefficients of the equations \ref{eq:cwe476} and
\ref{eq:cwe457} are low values, since the flaw rates are also low. It is
uncommon to find buggy functions in Linux \cite{ferreira2016}. So, when this
already low value is divided by the number of modules, which tends to increase
over time, we get very low values for the flaw rates.

Table \ref{tab:cwe} presents the defined models with the flaw rates in some
known points, as first collected version (2.6.11), last analyzed version (3.9),
the version with the higher flaw rates (2.6.39 for CWE476 and 3.16 for CWE457),
as well as, recent Linux versions for prediction points (4.0, 4.5, and 4.9).

In the one hand, the flaw rate for CWE476 (NULL pointer dereference) peaked in
version 2.6.39 and then started to decrease, but as soon as the number of
modules starts to increase, it tends to increase, reaching a new peak and finally
starting to decrease again. In the other hand, the flaw rate for CWE457
(Uninitialized variables) tends to decrease, as shown in Table \ref{tab:cwe}.
We can observe that the flaws always oscillate with a few peaks, but the values
tend to decrease. The values for CWE476 emphasize that fact, since the flaw
rate on the second peak is lower than the previous one.

After aplying the models to kernel versions later than 3.9, we compared the
real flaw rate values and the values estimated by our model. We obtained the
real values by dividing the number of occurences of CWE476 and CWE457 by the
number of modules of the respective kernel versions. The real values are shown
in Tables \ref{tab:cwe476new} and \ref{tab:cwe457new}

\begin{table}[h]
\centering
\begin{footnotesize}
\begin{tabular}{ccc}
\hline
{Version}  & {model\_tax\_CWE476} & {real\_tax\_CWE476} \\ \hline
linux-v4.0 & 0.006435337	 & 0.006085783   \\ \hline
linux-v4.5 & 0.006829370	 & 0.005968813    \\ \hline
linux-v4.9 & 0.007679160	 & 0.005729981    \\ \hline
\end{tabular}
  \caption{Comparação entre modelo e valor real CWE476}
\label{tab:cwe476new}
\end{footnotesize}
\end{table}


\begin{table}[h]
\centering
\begin{footnotesize}
\begin{tabular}{ccc}
\hline
{Version}  & {model\_tax\_CWE457} & {real\_tax\_CWE457} \\ \hline
linux-v4.0 & 0.006036167	 & 0.006562062   \\ \hline
linux-v4.5 & 0.005503281	 & 0.006615434    \\ \hline
linux-v4.9 & 0.004861321	 & 0.006179855    \\ \hline
\end{tabular}
	\caption{Comparação entre modelo e valor real para CWE457}
\label{tab:cwe457new}
\end{footnotesize}
\end{table}

By analyzing the real values and the values proposed by the models for Linux
versions greater than 3.9, we can see that the further the latest version used
to train the model is from the version analyzed, the greater the error on its
predictions. This leads to the need of updating the model for a certain
interval of new Linux releases, making it possible to reduce the error,
improving the precision of the statistical model predictions. An interesting
means to achieve such updates is by performing continuous static analysis on
software repositories (the kernel repository in this specific context) and
automate the model updating tasks. In the next Section we discuss our final
remarks and our plans for future work, including the development of a platform
to perform continuous static analysis on software repositories and make the
analyses available.
