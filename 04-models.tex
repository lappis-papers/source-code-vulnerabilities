\section{Low complexity models definition}\label{sec:models}

To achieve a lower error, we could use a polynomial function with a higher
degree. However, to build a suitable model, we need to avoid overfitting, the
extreme adjustment of the model to our data set, which would disturb the
extrapolation for future inputs. Our approach based on a non-parametric model
as reference helps avoiding a possible overfitting of the built model,
providing a more flexible model.

\begin{figure}[hbt]
 \centering
 \begin{minipage}[b]{0.475\textwidth}
  \includegraphics[width=\textwidth]{images/cwe476-all-models.eps}
  \caption{CWE 476 (Number of NULL pointer) data models prediction curve.}
  \label{fig:cwe476-all-models}
 \end{minipage}
 \begin{minipage}[b]{0.475\textwidth}
  \includegraphics[width=\textwidth]{images/cwe457-all-models.eps}
  \caption{CWE 476 (Use of uninitialized variable) data models prediction curve.}
  \label{fig:cwe457-all-models}
 \end{minipage}
\end{figure}

For the models definition, we identified and removed the outliers in the data
set, defined the non-parametric model with the LOESS method and, finally,
defined two parametric models through polynomial regression: one quadratic
polynomial and one cubic polynomial. In Figures \ref{fig:cwe476-all-models} and
\ref{fig:cwe457-all-models}, we present the ``real'' flaw rate values and flaws
evolution curves (``cubic'', ``quadratic'' and ``LOESS'') in the built models.

Figure \ref{fig:cwe476-all-models} shows that the built cubic model is the one
with the best approximation for the reference model curve (LOESS model). It is
especially good for data extrapolation on data sets higher or lower than the
data interval used in this study. On the edges, the cubic model has a better
approximation from the reference model, while the quadratic model tends to
diverge from it. Still, although all the models presented in Figure
\ref{fig:cwe457-all-models} have a good approximation for the reference model
curve, the cubic model fits the reference model better than the quadratic one,
both in the minimum and maximum limits.

\begin{table}[h]
 \centering
\begin{footnotesize}
 \begin{tabular}{ccc}
  \hline
  {Model} & {\textit{CWE}} & {Mean squared error} \\ \hline
  Quadratic   &  476  &    0.000000958                  \\ \hline
  Cubic       &  476  &    0.000000862                 \\ \hline
  Quadratic   &  457  &    0.000000137 \\ \hline
  Cubic       &  457  &    0.000000112 \\ \hline
 \end{tabular}
 \caption{Error obtained through cross-validation.}
 \label{tab:emq}
\end{footnotesize}
\end{table}

Finally, to compare the models, we performed a cross-validation with the K-fold
method to analyze the performance of both models in a data set that was not
used for training those models. In short, we used a ten-fold cross-validation
($K = 10$) for this study, which means dividing the sample in ten groups for
training and testing \cite{kohavi:1995}. Table \ref{tab:emq} presents the mean
squared error (MSE), associated to each of the models obtained through the
cross-validation. For instance, the MSE measures the quality of a prediction
model, so its values closer to zero are better, and they always are
non-negatives. Therefore, the low error ($MSE < 0.000001$), obtained from the
presented functions, guarantees the models reliability.

For both CWEs, the cubic model stood out. The error associated with the
quadratic model for CWE476 was approximately 11.14\% higher than the cubic
model in ``real'' analysis situations, while the error associated with the
quadratic model for CWE457 was approximately 22.32\% higher than the cubic
model\footnote{These numbers can be verified by comparing the values in Table
\ref{tab:emq}, dividing the quadratic error values by the cubic ones.}.
Therefore, in both cases, the cubic models are better for predicting the Linux
flaw rate evolution, since they show a better approximation from the
reference models when it comes to the extrapolation of the analyzed data
boundaries.

